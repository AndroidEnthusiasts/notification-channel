package com.rba.notificationchannel.common;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Color;
import android.os.Build;

/**
 * Created by Administrator on 2/6/2018.
 */

public class NotificationUtils extends ContextWrapper {
    private NotificationManager mManager;
    public static final String DEFAULT_CHANNEL_ID = "default_id";
    public static final String CUSTOMIZED_CHANNEL_ID = "customized_id";
    public static final String DEFAULT_CHANNEL_NAME = "default channel";
    public static final String CUSTOMIZED_CHANNEL_NAME = "customized channel";

    public NotificationUtils(Context base) {
        super(base);
        //alterNotificationChannel();
        createChannels();
    }

    public void createChannels() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel defaultChannel = new NotificationChannel(DEFAULT_CHANNEL_ID, DEFAULT_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            defaultChannel.enableLights(true);
            defaultChannel.enableVibration(true);
            defaultChannel.setLightColor(Color.DKGRAY);
            defaultChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            getManager().createNotificationChannel(defaultChannel);

            // create custom channel
            NotificationChannel customizedChannel = new NotificationChannel(CUSTOMIZED_CHANNEL_ID,
                    CUSTOMIZED_CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
            customizedChannel.enableLights(true);
            customizedChannel.enableVibration(true);
            customizedChannel.setLightColor(Color.GRAY);
            customizedChannel.setShowBadge(false);
            customizedChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            getManager().createNotificationChannel(customizedChannel);
        }
    }

    public NotificationManager getManager() {
        if (mManager == null) {
            mManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return mManager;
    }

    public Notification.Builder getDefaultChannelNotification(String title, String body) {
        Notification.Builder builder = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = new Notification.Builder(getApplicationContext(), DEFAULT_CHANNEL_ID)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setSmallIcon(android.R.drawable.stat_notify_more)
                    .setAutoCancel(true);
        } else {
            builder = new Notification.Builder(getApplicationContext())
                    .setContentTitle(title)
                    .setContentText(body)
                    .setSmallIcon(android.R.drawable.stat_notify_more);
        }
        return builder;
    }

    public Notification.Builder getCustomizedChannelNotification(String title, String body) {
        Notification.Builder builder = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = new Notification.Builder(getApplicationContext(), CUSTOMIZED_CHANNEL_ID)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setSmallIcon(android.R.drawable.stat_notify_more)
                    .setColor(Color.BLACK)
                    .setAutoCancel(true);
            /*.setTimeoutAfter(2000)*/
        } else {
            builder = new Notification.Builder(getApplicationContext())
                    .setContentTitle(title)
                    .setContentText(body)
                    .setSmallIcon(android.R.drawable.stat_notify_more);
        }
        return builder;
    }

}
