package com.rba.notificationchannel;

import android.app.Notification;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.rba.notificationchannel.common.NotificationUtils;

public class MainActivity extends AppCompatActivity {
    private NotificationUtils mNotificationUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mNotificationUtils = new NotificationUtils(this);
        defaultChannel();
        customizedChannel();
        navigateToDefaultChannelSettings();
        navigateToCustomizeChannelSettings();
    }

    private void defaultChannel() {
        final EditText editTextTitleAndroid =  findViewById(R.id.et_default_title);
        final EditText editTextAuthorAndroid =  findViewById(R.id.et_gefault_author);
        Button buttonAndroid = (Button) findViewById(R.id.btn_send_default);

        buttonAndroid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = editTextTitleAndroid.getText().toString();
                String author = editTextAuthorAndroid.getText().toString();

                if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(author)) {
                    Notification.Builder nb = mNotificationUtils.getDefaultChannelNotification(title, "By " + author);

                    mNotificationUtils.getManager().notify(101, nb.build());
                }
            }
        });
    }

    private void customizedChannel() {
        final EditText editTextTitleIos =  findViewById(R.id.et_customize_title);
        final EditText editTextAuthorIos =  findViewById(R.id.et_customize_author);
        Button buttonIos =  findViewById(R.id.btn_send_customiz);
        buttonIos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = editTextTitleIos.getText().toString();
                String author = editTextAuthorIos.getText().toString();

                if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(author)) {
                    Notification.Builder nb = mNotificationUtils
                            .getCustomizedChannelNotification(title, "By " + author);

                    mNotificationUtils.getManager().notify(102, nb.build());
                }
            }
        });
    }

    private void navigateToDefaultChannelSettings() {
        Button buttonAndroidNotifSettings = (Button) findViewById(R.id.btn_default_notif_settings);
        buttonAndroidNotifSettings.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent i = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    i = new Intent(Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS);
                    i.putExtra(Settings.EXTRA_APP_PACKAGE, getPackageName());
                    i.putExtra(Settings.EXTRA_CHANNEL_ID, NotificationUtils.DEFAULT_CHANNEL_ID);
                    startActivity(i);
                } else {
                    i = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.parse("package:" + getPackageName()));
                    i.addCategory(Intent.CATEGORY_DEFAULT);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
            }
        });
    }

    private void navigateToCustomizeChannelSettings() {
        Button buttonAndroidNotifSettings = (Button) findViewById(R.id.btn_customize_notif_settings);
        buttonAndroidNotifSettings.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent i = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    i = new Intent(Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS);
                    i.putExtra(Settings.EXTRA_APP_PACKAGE, getPackageName());
                    i.putExtra(Settings.EXTRA_CHANNEL_ID, NotificationUtils.CUSTOMIZED_CHANNEL_ID);
                    startActivity(i);
                } else {
                    i = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.parse("package:" + getPackageName()));
                    i.addCategory(Intent.CATEGORY_DEFAULT);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
            }
        });
    }
}
